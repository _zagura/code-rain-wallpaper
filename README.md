# Code Rain Wallpaper

A live wallpaper inspired by the matrix falling code effect.
It shows a bunch of falling sitelen pona characters, you can adjust the background, foreground, accent & glow colors.

The font used is from https://github.com/ETBCOR/nasin-nanpa
