package one.zagura.CodeRainWall

import android.app.WallpaperManager
import android.content.ComponentName
import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.*
import androidx.compose.runtime.*
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import one.zagura.CodeRainWall.ui.ColorSetting
import one.zagura.CodeRainWall.ui.TitleBar
import one.zagura.CodeRainWall.ui.theme.CodeRainTheme

class SettingsActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            CodeRainTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colors.surface
                ) {
                    Column(
                        modifier = Modifier
                            .verticalScroll(rememberScrollState())
                    ) {
                        TitleBar(stringResource(id = R.string.app_name))
                        val index = remember { mutableIntStateOf(-1) }
                        ColorSetting(0, index, R.string.background, BACKGROUND_COLOR_KEY, BACKGROUND_COLOR_DEFAULT)
                        ColorSetting(1, index, R.string.foreground, FOREGROUND_COLOR_KEY, FOREGROUND_COLOR_DEFAULT)
                        ColorSetting(2, index, R.string.accent, ACCENT_COLOR_KEY, ACCENT_COLOR_DEFAULT)
                        ColorSetting(3, index, R.string.glow, GLOW_COLOR_KEY, GLOW_COLOR_DEFAULT)
                        Button(
                            modifier = Modifier
                                .align(Alignment.CenterHorizontally)
                                .padding(top = 16.dp, bottom = 48.dp),
                            onClick = {
                                val intent = Intent(WallpaperManager.ACTION_CHANGE_LIVE_WALLPAPER)
                                intent.putExtra(
                                    WallpaperManager.EXTRA_LIVE_WALLPAPER_COMPONENT,
                                    ComponentName(applicationContext, CodeRainWallService::class.java)
                                )
                                startActivity(intent)
                            }
                        ) {
                            Text(text = getString(R.string.set_wallpaper))
                        }
                    }
                }
            }
        }
    }
}
