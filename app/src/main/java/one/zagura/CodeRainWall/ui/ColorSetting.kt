package one.zagura.CodeRainWall.ui

import android.content.Intent
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.defaultMinSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.MutableIntState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import com.godaddy.android.colorpicker.ClassicColorPicker
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import one.zagura.CodeRainWall.CodeRainWallService
import one.zagura.CodeRainWall.colorSettings

@Composable
fun ColorSetting(index: Int, state: MutableIntState, labelID: Int, key: Preferences.Key<Int>, default: Int) {
    val scope = rememberCoroutineScope()
    val context = LocalContext.current
    val color by remember {
        context.colorSettings.data.map {
            it[key] ?: default
        }
    }.collectAsState(initial = 0)

    val colorValue = Color(color or 0xff000000.toInt())

    val isOpen = state.intValue == index
    Row(
        modifier = Modifier
            .fillMaxWidth(1f)
            .clickable(onClick = {
                state.intValue = if (isOpen) -1 else index
            })
            .padding(horizontal = 24.dp, vertical = 16.dp)
    ) {
        Text(
            text = stringResource(labelID),
            modifier = Modifier
                .weight(1f)
                .align(Alignment.CenterVertically),
        )
        Surface(
            color = colorValue,
            shape = CircleShape,
            modifier = Modifier
                .size(32.dp)
                .align(Alignment.CenterVertically)
                .border(1.dp, Color.Black, CircleShape),
        ) {}
    }
    if (isOpen) {
        ClassicColorPicker(
            color = colorValue,
            showAlphaBar = false,
            modifier = Modifier
                .defaultMinSize(minHeight = 128.dp)
                .aspectRatio(3f / 2f, true)
                .padding(start = 24.dp, end = 24.dp, bottom = 24.dp),
            onColorChanged = {
                scope.launch {
                    context.colorSettings.edit { p ->
                        p[key] = it.toColor().toArgb()
                    }
                    context.startService(Intent(context, CodeRainWallService::class.java))
                }
            }
        )
    }
    Divider(color = Color.Black, thickness = 1.dp)
}