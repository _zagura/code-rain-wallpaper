package one.zagura.CodeRainWall.ui.theme

import android.app.Activity
import android.content.Context
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.remember
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalView
import androidx.core.graphics.ColorUtils
import androidx.core.view.WindowCompat
import kotlinx.coroutines.flow.map
import one.zagura.CodeRainWall.ACCENT_COLOR_DEFAULT
import one.zagura.CodeRainWall.ACCENT_COLOR_KEY
import one.zagura.CodeRainWall.BACKGROUND_COLOR_DEFAULT
import one.zagura.CodeRainWall.BACKGROUND_COLOR_KEY
import one.zagura.CodeRainWall.colorSettings

@Composable
fun Context.CodeRainTheme(
    content: @Composable () -> Unit
) {
    val backgroundColor = remember {
        colorSettings.data.map {
            it[BACKGROUND_COLOR_KEY] ?: BACKGROUND_COLOR_DEFAULT
        }
    }.collectAsState(initial = 0)
    val accentColor = remember {
        colorSettings.data.map {
            it[ACCENT_COLOR_KEY] ?: ACCENT_COLOR_DEFAULT
        }
    }.collectAsState(initial = 0)
    val bg = Color(backgroundColor.value or 0xff000000.toInt())
    val accent = Color(accentColor.value or 0xff000000.toInt())
    val l = DoubleArray(3).apply { ColorUtils.colorToLAB(backgroundColor.value, this) }[0]
    val isDark = l < 50f
    val bgDarkened = Color(ColorUtils.blendARGB(bg.toArgb(), 0xff000000.toInt(), 0.5f))
    val colors = if (isDark) darkColors(
        background = bgDarkened,
        surface = bg,
        primary = accent,
        secondary = accent,
        onPrimary = bg,
        onSecondary = bg,
    ) else lightColors(
        background = bgDarkened,
        surface = bg,
        primary = accent,
        secondary = accent,
        onPrimary = bg,
        onSecondary = bg,
    )
    val view = LocalView.current
    if (!view.isInEditMode) {
        SideEffect {
            val window = (view.context as Activity).window
            window.statusBarColor = bgDarkened.toArgb()
            WindowCompat.getInsetsController(window, view).isAppearanceLightStatusBars = !isDark
        }
    }

    MaterialTheme(
        colors = colors,
        typography = Typography,
        content = content
    )
}