package one.zagura.CodeRainWall

import android.app.WallpaperColors
import android.content.Context
import android.content.Intent
import android.graphics.*
import android.os.Build
import android.os.Handler
import android.os.Looper
import android.service.wallpaper.WallpaperService
import android.view.SurfaceHolder
import androidx.annotation.RequiresApi
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.luminance
import androidx.core.graphics.toColor
import androidx.core.graphics.toXfermode
import androidx.datastore.preferences.core.intPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.launch
import kotlin.math.abs
import kotlin.random.Random

val Context.colorSettings by preferencesDataStore("color")

val BACKGROUND_COLOR_KEY = intPreferencesKey("cBG")
val FOREGROUND_COLOR_KEY = intPreferencesKey("cFG")
val ACCENT_COLOR_KEY = intPreferencesKey("cAccent")
val GLOW_COLOR_KEY = intPreferencesKey("cGlow")

const val BACKGROUND_COLOR_DEFAULT = 0x111111
const val FOREGROUND_COLOR_DEFAULT = 0x999999
const val ACCENT_COLOR_DEFAULT = 0xffffff
const val GLOW_COLOR_DEFAULT = 0xdddddd

class CodeRainWallService : WallpaperService() {

    private var engine: Engine? = null

    override fun onCreateEngine() = Engine().also { engine = it }
    private val frameRate = 24

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        engine?.loadColors()
        return super.onStartCommand(intent, flags, startId)
    }

    companion object {
        private const val trailBlurF = 0.05f
        private const val blurF = 0.04f
        private const val glowF = 1.2f
    }

    inner class Engine : WallpaperService.Engine() {

        private var viewWidth = 0
        private var viewHeight = 0

        private var yPositions = IntArray(0)
        private var xOffset = 0f
        private var maxI = 0
        private var waitUntil = DoubleArray(0)
        private var lastRedX = 0
        private var lastRedY = 0
        private var cellWidth = 64f
        private var cellHeight = 64f

        private var clearColor = 0
        private var fullClearColor = 0
        private val charPaint = Paint().apply {
            style = Paint.Style.FILL
            typeface = ResourcesCompat.getFont(this@CodeRainWallService, R.font.nasin_nanpa)
            textSize = cellHeight * 0.9f
            maskFilter = BlurMaskFilter(cellHeight * blurF, BlurMaskFilter.Blur.NORMAL)
        }
        private val accentTrailPaint = Paint().apply {
            style = Paint.Style.FILL
            typeface = ResourcesCompat.getFont(this@CodeRainWallService, R.font.nasin_nanpa)
            textSize = cellHeight * 0.9f
            maskFilter = BlurMaskFilter(cellHeight * trailBlurF, BlurMaskFilter.Blur.NORMAL)
        }
        private val accentPaint = Paint().apply {
            style = Paint.Style.FILL
            typeface = ResourcesCompat.getFont(this@CodeRainWallService, R.font.nasin_nanpa)
            textSize = cellHeight * 0.9f
            maskFilter = BlurMaskFilter(cellHeight * blurF, BlurMaskFilter.Blur.NORMAL)
        }
        private val glowPaint = Paint().apply {
            style = Paint.Style.FILL
            typeface = ResourcesCompat.getFont(this@CodeRainWallService, R.font.nasin_nanpa)
            textSize = cellHeight * 0.9f
            maskFilter = BlurMaskFilter(cellHeight * glowF, BlurMaskFilter.Blur.NORMAL)
            xfermode = PorterDuff.Mode.ADD.toXfermode()
        }

        private val drawHandler = Handler(Looper.myLooper()!!)
        private var visible = false

        init {
            loadColors()
        }

        fun loadColors() {
            MainScope().launch {
                val data = colorSettings.data.first()
                val backgroundColor = (data[BACKGROUND_COLOR_KEY] ?: BACKGROUND_COLOR_DEFAULT) and 0xffffff
                val foregroundColor = (data[FOREGROUND_COLOR_KEY] ?: FOREGROUND_COLOR_DEFAULT) and 0xffffff
                clearColor = backgroundColor or 0xdd000000.toInt()
                fullClearColor = backgroundColor or 0xff000000.toInt()
                charPaint.color = foregroundColor or 0x88000000.toInt()
                accentTrailPaint.color = foregroundColor or 0xff000000.toInt()
                accentPaint.color = (data[ACCENT_COLOR_KEY] ?: ACCENT_COLOR_DEFAULT) or 0xff000000.toInt()
                glowPaint.color = (data[GLOW_COLOR_KEY] ?: GLOW_COLOR_DEFAULT) or 0xff000000.toInt()
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
                    notifyColorsChanged()
                }
            }
        }

        private fun setDimensions() {
            val cells = viewWidth / cellWidth
            yPositions = IntArray(cells.toInt())
            waitUntil = DoubleArray(cells.toInt())
            xOffset = (cells % 1) * cellWidth
            maxI = (viewHeight / cellHeight).toInt()
        }

        private fun fullClear(canvas: Canvas) = canvas.drawColor(fullClearColor)

        private fun clear(canvas: Canvas) = canvas.drawColor(clearColor)

        private fun draw(canvas: Canvas) {
            for (i in yPositions.indices) {
                val text = String(Character.toChars(0xf1900 + (Random((System.currentTimeMillis().toInt() / 60) xor i).nextFloat () * 0x88).toInt()))
                val paint = if (abs(lastRedY + lastRedX - i - yPositions[i]) > 3) {
                    lastRedX = i
                    lastRedY = yPositions[i]
                    charPaint
                } else accentPaint
                if (System.currentTimeMillis() - waitUntil[i] > 0) {
                    val trailPaint = if (paint == accentPaint)
                        accentTrailPaint
                    else charPaint
                    if (Random.nextFloat () > 0.3) {
                        waitUntil[i] = 0.0
                        drawText (canvas, text, i, trailPaint)
                        if (yPositions[i] > maxI)
                            yPositions[i] = 0
                        else
                            yPositions[i]++
                    } else
                        waitUntil[i] = System.currentTimeMillis() + Random.nextDouble() * 1200 + 300
                } else {
                    if (paint == accentPaint)
                        drawText(canvas, text, i, glowPaint)
                    drawText(canvas, text, i, paint)
                }
            }
        }

        private fun drawText(canvas: Canvas, text: String, i: Int, paint: Paint) {
            canvas.drawText(text, xOffset + i * cellWidth, (yPositions[i] + 1) * cellHeight, paint)
        }

        private fun frame() {
            val holder = surfaceHolder
            Thread.sleep(10)
            holder.lockCanvas().also {
                clear(it)
                draw(it)
            }.run(holder::unlockCanvasAndPost)
            drawHandler.removeCallbacks(::frame)
            if (visible) {
                drawHandler.postDelayed(::frame, 1000L / frameRate)
            }
        }

        override fun onDestroy() {
            drawHandler.removeCallbacks(::frame)
        }

        override fun onVisibilityChanged(visible: Boolean) {
            this.visible = visible
            if (visible) frame()
            else drawHandler.removeCallbacks(::frame)
        }

        override fun onSurfaceCreated(holder: SurfaceHolder) {
            loadColors()
            setDimensions()
            holder.lockCanvas().apply(::fullClear).run(holder::unlockCanvasAndPost)
            frame()
        }

        override fun onSurfaceChanged(
            holder: SurfaceHolder,
            format: Int, width: Int, height: Int
        ) {
            viewWidth = width
            viewHeight = height
            loadColors()
            setDimensions()
            holder.lockCanvas().apply(::fullClear).run(holder::unlockCanvasAndPost)
            frame()
        }

        override fun onSurfaceDestroyed(holder: SurfaceHolder?) {
            visible = false
            drawHandler.removeCallbacks(::frame)
        }

        @RequiresApi(Build.VERSION_CODES.O_MR1)
        override fun onComputeColors(): WallpaperColors {
            val background = fullClearColor
            val foreground = charPaint.color or 0xff000000.toInt()
            val accent = accentPaint.color
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.S) WallpaperColors(
                background.toColor(),
                foreground.toColor(),
                accent.toColor(),
                if (background.luminance < 0.3f) WallpaperColors.HINT_SUPPORTS_DARK_THEME
                else WallpaperColors.HINT_SUPPORTS_DARK_TEXT
            ) else WallpaperColors(
                background.toColor(),
                foreground.toColor(),
                accent.toColor(),
            )
        }
    }
}